﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using CLSTA.Models;
using CLSTA.Data;
using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;

namespace CLSTA.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly ApplicationDbContext _context;

        public HomeController(ILogger<HomeController> logger, ApplicationDbContext context) {
            _logger = logger;
            _context = context;
        }
        
        public IActionResult Index([Bind("Area,MinPrice,MaxPrice,MinBathrooms,MaxBathrooms,MinBedrooms,MaxBedrooms,Features")]SearchViewModel search, List<int> selectedFeatures) {

            if (search == null) {
                search = new SearchViewModel {
                    Listings = _context.Listings.ToList()
                };

                return View(search);
            }

            search.Features = 0;
            if (selectedFeatures != null) {
                for (int i = 0; i < selectedFeatures.Count; i++) {
                    search.Features |= (Feature)selectedFeatures[i];
                }
            }

            IEnumerable<Listing> listings;
            if (string.IsNullOrEmpty(search.Area)) {
                listings = _context.Listings.Where(x => x.Visible);
            } else {
                listings = _context.Listings.Where(x => x.Address.ToUpper().Contains(search.Area.ToUpper()) && x.Visible);
            }

            if (search.MinPrice > 0)
                listings = listings.Where(x => x.PcmPrice > search.MinPrice);

            if (search.MaxPrice > 0)
                listings = listings.Where(x => x.PcmPrice < search.MaxPrice);

            if (search.MinBedrooms > 0)
                listings = listings.Where(x => x.BedroomCount > search.MinBedrooms);

            if (search.MaxBedrooms > 0)
                listings = listings.Where(x => x.BedroomCount < search.MaxBedrooms);

            if (search.MinBathrooms > 0)
                listings = listings.Where(x => x.BathroomCount > search.MinBathrooms);

            if (search.MaxBathrooms > 0)
                listings = listings.Where(x => x.BathroomCount < search.MaxBathrooms);

            if (search.Features != 0)
                listings = listings.Where(x => (x.Features | search.Features) == search.Features);

            search.Listings = listings.ToList();

            if (listings != null) {
                search.Images = new List<string>();
                foreach(Listing listing in listings) {
                    Image img = _context.Images.FirstOrDefault(x => x.ListingId == listing.Id);
                    search.Images.Add(img == null ? "https://via.placeholder.com/250" : string.Format("{0}://{1}/Images/{2}", Request.Scheme, Request.Host, img.Url));
                }
            }

            return View(search);
        }

        public IActionResult Listing(int? id) {
            if (id == null) {
                return NotFound();
            }

            var listing = _context.Listings
                .FirstOrDefault(m => m.Id == id);
            if (listing == null) {
                return NotFound();
            }

            listing.Images = _context.Images.Where(x => x.ListingId == id).ToList();

            return View(listing);
        }

        [Authorize]
        public IActionResult Enquire(int? id) {

            if (id == null) {
                return NotFound();
            }

            var listing = _context.Listings
                .FirstOrDefault(m => m.Id == id);
            if (listing == null) {
                return NotFound();
            }


            return View(id);
        }

        [Authorize]
        [HttpPost]
        public IActionResult Enquire(int? listingId, string note) {
            if (listingId == null) {
                return NotFound();
            }

            var listing = _context.Listings
                .FirstOrDefault(m => m.Id == listingId);
            if (listing == null) {
                return NotFound();
            }

            string userId = User.FindFirst(ClaimTypes.NameIdentifier).Value;

            NoteOfInterest noi = new NoteOfInterest {
                ListingId = listing.Id,
                UserId = userId,
                Notes = note
            };

            _context.Add(noi);

            return RedirectToAction("Listing", new { id = listingId.Value });
        }

        public IActionResult Privacy() {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error() {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
