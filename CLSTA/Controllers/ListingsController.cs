﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using CLSTA.Data;
using CLSTA.Models;
using Microsoft.AspNetCore.Http;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;

namespace CLSTA.Controllers
{
    [Authorize(Roles = "Administrator")]
    public class ListingsController : Controller
    {
        private readonly IWebHostEnvironment _env;
        private readonly ApplicationDbContext _context;
        private readonly UserManager<IdentityUser> _usr;

        public ListingsController(IWebHostEnvironment env, ApplicationDbContext context, UserManager<IdentityUser> _usr)
        {
            _env = env;
            _context = context;
        }

        // GET: Listings
        public async Task<IActionResult> Index()
        {
            return View(await _context.Listings.ToListAsync());
        }

        // GET: Listings/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var listing = await _context.Listings
                .FirstOrDefaultAsync(m => m.Id == id);
            if (listing == null)
            {
                return NotFound();
            }

            listing.Images = _context.Images.Where(x => x.ListingId == id).ToList();
            listing.NotesOfInterest = _context.NotesOfInterest.Where(x => x.ListingId == id).ToList();

            List<string> userNames = new List<string>();
            if (listing.NotesOfInterest != null) {
                for (int i = 0; i < listing.NotesOfInterest.Count; i++) {

                    var user = await _usr.FindByIdAsync(listing.NotesOfInterest[i].UserId);

                    userNames.Add(user.UserName);
                }
            }

            ViewBag.UserNames = userNames;

            return View(listing);
        }

        // GET: Listings/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Listings/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,AddressLine1,AddressLine2,AddressCity,AddressCounty,AddressCountry,Postcode,ShortDescription,LongDescription,BedroomCount,BathroomCount,DateAvailable,PcmPrice,Features,Visible")] Listing listing, List<int> selectedFeatures)
        {
            if (ModelState.IsValid)
            {
                listing.Features = 0;
                if (selectedFeatures != null) {
                    for (int i = 0; i < selectedFeatures.Count; i++) {
                        listing.Features |= (Feature)selectedFeatures[i];
                    }
                }

                _context.Add(listing);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(listing);
        }

        // GET: Listings/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var listing = await _context.Listings.FindAsync(id);
            if (listing == null)
            {
                return NotFound();
            }
            return View(listing);
        }

        // POST: Listings/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,AddressLine1,AddressLine2,AddressCity,AddressCounty,AddressCountry,Postcode,ShortDescription,LongDescription,BedroomCount,BathroomCount,DateAvailable,PcmPrice,Features,Visible")] Listing listing, List<int> selectedFeatures)
        {
            if (id != listing.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    listing.Features = 0;
                    if (selectedFeatures != null) {
                        for (int i = 0; i < selectedFeatures.Count; i++) {
                            listing.Features |= (Feature)selectedFeatures[i];
                        }
                    }

                    _context.Update(listing);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ListingExists(listing.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(listing);
        }

        // GET: Listings/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var listing = await _context.Listings
                .FirstOrDefaultAsync(m => m.Id == id);
            if (listing == null)
            {
                return NotFound();
            }

            return View(listing);
        }

        // POST: Listings/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var listing = await _context.Listings.FindAsync(id);
            _context.Listings.Remove(listing);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        // GET: Listings/UploadImage/5
        public async Task<IActionResult> UploadImage(int? id) {

            if (id == null) {
                return NotFound();
            }

            var listing = await _context.Listings
                .FirstOrDefaultAsync(m => m.Id == id);
            if (listing == null) {
                return NotFound();
            }

            return View(id);
        }

        // POST: Listings/UploadImage
        [HttpPost]
        public async Task<IActionResult> UploadImage(int? id, List<IFormFile> files) {

            if (ModelState.IsValid) {

                if (id == null) {
                    return NotFound();
                }

                var listing = await _context.Listings.FindAsync(id);
                if (listing == null) {
                    return NotFound();
                }

                string imagesDirectory = Path.Combine(_env.WebRootPath, "Images");
                
                if (!Directory.Exists(imagesDirectory))
                    Directory.CreateDirectory(imagesDirectory);

                long size = files.Sum(x => x.Length);

                foreach (var file in files) {
                    if (file.Length > 0) {

                        string fileExtension = Path.GetExtension(file.FileName);
                        string fileName = string.Format("{0}{1}", Path.GetRandomFileName(), fileExtension);
                        string path = Path.Combine(imagesDirectory, fileName);

                        using (var stream = System.IO.File.Create(path)) {
                            await file.CopyToAsync(stream);
                        }

                        Image image = new Image {
                            ListingId = id.Value,
                            Url = fileName
                        };

                        _context.Add(image);

                        if (listing.Images == null)
                            listing.Images = new List<Image>();

                        listing.Images.Add(image);
                    }
                }

                _context.Update(listing);
                _context.SaveChanges();

                return RedirectToAction("Index");
            }

            return BadRequest();
        }

        private bool ListingExists(int id)
        {
            return _context.Listings.Any(e => e.Id == id);
        }
    }
}
