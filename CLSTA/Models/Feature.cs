﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CLSTA.Models
{
    [Flags]
    public enum Feature
    {
        Garden = (1 << 0),
        Garage = (1 << 1),
        Fireplace = (1 << 2)
    }
}
