﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CLSTA.Models
{
    public class Image
    {
        public int Id { get; set; }

        public int ListingId { get; set; }

        [DataType(DataType.ImageUrl)]
        public string Url { get; set; }
    }
}
