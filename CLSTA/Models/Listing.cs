﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CLSTA.Models
{
    public class Listing
    {
        public int Id { get; set; }

        public string Address {
            get {
                string line = string.IsNullOrEmpty(AddressLine2) ? AddressLine1 : string.Format("{0}, {1}", AddressLine1, AddressLine2);
                return string.Format("{0}, {1}, {2}, {3}, {4}", line, AddressCity, AddressCounty, AddressCountry, Postcode);
            }
        }

        [Display(Name="Line 1")]
        public string AddressLine1 { get; set; }
        [Display(Name = "Line 2")]
        public string AddressLine2 { get; set; }
        [Display(Name = "City")]
        public string AddressCity { get; set; }
        [Display(Name = "County")]
        public string AddressCounty { get; set; }
        [Display(Name = "Country")]
        public string AddressCountry { get; set; }
        [Display(Name = "Postcode")]
        public string Postcode { get; set; }

        [Display(Name = "Short Description")]
        public string ShortDescription { get; set; }
        [Display(Name = "Long Description")]
        public string LongDescription { get; set; }

        [Display(Name = "Bedrooms")]
        public int BedroomCount { get; set; }
        [Display(Name = "Bathrooms")]
        public int BathroomCount { get; set; }

        [Display(Name = "Date Available")]
        [DataType(DataType.Date)]
        public DateTime DateAvailable { get; set; }

        [Display(Name ="Per Calendar Month (£)")]
        [DataType(DataType.Currency)]
        public decimal PcmPrice { get; set; }

        public Feature Features { get; set; }
        public List<Image> Images { get; set; }
        public List<NoteOfInterest> NotesOfInterest { get; set; }

        public bool Visible { get; set; }
    }
}
