﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CLSTA.Models
{
    public class NoteOfInterest
    {
        public int Id { get; set; }

        public string UserId { get; set; }

        public int ListingId { get; set; }

        [Display(Name = "Notes/Comments")]
        public string Notes { get; set; }
    }
}
