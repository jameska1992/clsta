﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CLSTA.Models
{
    public class SearchViewModel
    {
        public string Area { get; set; }
        public decimal MinPrice { get; set; }
        public decimal MaxPrice { get; set; }
        public int MinBedrooms { get; set; }
        public int MaxBedrooms { get; set; }
        public int MinBathrooms { get; set; }
        public int MaxBathrooms { get; set; }
        public Feature Features { get; set; }
        public List<Listing> Listings { get; set; }
        public List<string> Images { get; set; }
    }
}
