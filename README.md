# Technical Assessment Report #
## By James Anderson ##

### Overview ###
We required a new customer portal be created for a property letting agency, the agencies customers require an online system to allow them to register interest in properties available on the market.
Technology
The systems backend will be developed using ASP.NET Core MVC with a MSSQL database, frontend will use jQuery and bootstrap.

As this requires user accounts the Microsoft ASP.NET Identity will be used.

The web portal is to be hosted on a cloud service such as AWS or Azure.

### Security ###
Using the tools available:
All user inputs have been sanitized.
Anti forgery keys have been used where applicable.
Linq to SQL has been used to prevent SQL injection attacks.
Session keys and password hashing are used.
The admin functionality is locked down to only those users in the “Admin” role

### Functionality ###
The following functionality is to be included:
Provide a list of properties available for rent
User can create an enquiry to not their interest in a property
User can register account

### Assumptions ###
During the design and development process, I have made the following assumptions:
Listings will be from within the UK
Typical data points will be used for the listings (e.g. Number of bedrooms)
The first image uploaded for a listing will be the primary image and used when only a single image is called for
Further Improvements and Considerations

Much of the visuals are sorely lacking due to a lack of branding content or knowledge of the client company.

The listing presentation page needs a lot of work to better present the data and images. This would greatly benefit from having a lightbox/gallery.

The ability to upload and manage images during creation/editing of a listing would greatly increase the flow and usability of the system.

There is currently no way of managing users.

The enquire page could be moved to a popup modal on the listing presentation page.

Displaying if the current user has already made an enquiry on a listing and preventing them from creating another will reduce the amount of redundant enquiries.

The site would greatly benefit from having an area where the users can see any enquiries they have made.

An email or other notification should be raised when an enquiry is made.

### Host ###
https://clsta20200827150039.azurewebsites.net

Admin Credentials:
	username: admin@admin.com
	password: P@ssword1!